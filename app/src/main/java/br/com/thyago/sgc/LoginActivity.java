package br.com.thyago.sgc;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.thyago.sgc.databinding.ActivityLoginBinding;
import br.com.thyago.sgc.model.Aluno;
import br.com.thyago.sgc.util.ToastMessagesUtil;
import br.com.thyago.sgc.viewmodel.LoginViewModel;
import br.com.thyago.sgc.viewmodel.StatusLogin;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_CONTACTS;


public class LoginActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_CODE = 0;

    private EditText mCpfView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    LoginViewModel loginViewModel;
    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = new LoginViewModel();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setLoginVm(loginViewModel);
        binding.executePendingBindings();

        mCpfView = findViewById(R.id.email);

        requisitarPermissoes();

        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        bindLoadingState();
        bindLoginStatus();
    }

    private boolean requisitarPermissoes() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(INTERNET) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(INTERNET)) {
            Snackbar.make(mCpfView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_PERMISSION_CODE);
                        }
                    });
        } else {
            requestPermissions(new String[]{INTERNET, ACCESS_NETWORK_STATE}, REQUEST_PERMISSION_CODE);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requisitarPermissoes();
            }
        }
    }

    public void bindLoadingState() {
        binding.getLoginVm().loadingState.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                boolean isLoading = binding.getLoginVm().loadingState.get();
                if (isLoading) {
                    showProgress(true);
                } else {
                    showProgress(false);
                }
            }
        });
    }

    public void bindLoginStatus() {
        binding.getLoginVm().statusLogin.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                StatusLogin statusLogin = binding.getLoginVm().statusLogin.get();
                if (statusLogin == StatusLogin.SUCCESSO) {
                    Aluno aluno = binding.getLoginVm().alunoObservable.get();
                    Intent it = new Intent(LoginActivity.this, MenuActivity.class);
                    it.putExtra("alunoParamExtra", aluno);

                    startActivity(it);
                    finish();
                } else if (statusLogin == StatusLogin.ERRO_CONEXAO) {
                    ToastMessagesUtil.setToast(LoginActivity.this, "Não foi possível se conectar ao serviço. Tente novamente mais tarde.", true);
                } else if (statusLogin == StatusLogin.NAO_AUTORIZADO) {
                    ToastMessagesUtil.setToast(LoginActivity.this, "Usuário/Senha inválidos.", true);
                }
            }
        });
    }

    private void attemptLogin() {
        // Reset errors.
        mCpfView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mCpfView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mCpfView.setError(getString(R.string.error_field_required));
            focusView = mCpfView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {

            loginViewModel.onLoginClick();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 3;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


}

