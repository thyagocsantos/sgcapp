package br.com.thyago.sgc.exceptions;

public class WebApplicationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private int statusCode;

    public WebApplicationException(String mensagem) {
        super(mensagem);
    }

    public WebApplicationException(Throwable throwable) {
        super(throwable);
    }

    public WebApplicationException(String msg, Throwable e) {
        super(msg, e);
    }

    public WebApplicationException(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
