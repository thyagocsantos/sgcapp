package br.com.thyago.sgc.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import br.com.thyago.sgc.exceptions.WebApplicationException;

public class RestUtil {
    private static final String MIME_TYPE_DEFAULT = "application/json;charset=ISO-8859-1";
    private static final String CHARSET_DEFAULT = "ISO-8859-1";

    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    private static int HTTP_COD_SUCESSO = 200;

    private static void createSocketSSL() {
        final X509TrustManager cert = new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                return;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                return;
            }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new X509TrustManager[] { cert }, null);

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            final HostnameVerifier hv = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static URLConnection makeRequest(String method, String address, String mimeType, String charset,
                                     String requestBody) throws IOException {
        address = address.replaceAll(" ", "%20");
        URL url = new URL(address);

        URLConnection urlConnection;

        if (isAcessoSeguro(address)) {
            createSocketSSL();
            urlConnection = (HttpsURLConnection) url.openConnection();
            ((HttpsURLConnection) urlConnection).setRequestMethod(method);
        } else {
            urlConnection = (HttpURLConnection) url.openConnection();
            ((HttpURLConnection)urlConnection).setRequestMethod(method);
        }

        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(requestBody != null);
        urlConnection.setConnectTimeout(60000);
        urlConnection.setReadTimeout(60000);

        urlConnection.setRequestProperty("Content-type", mimeType);
        urlConnection.setRequestProperty("Accept-Charset", charset);

        if (requestBody != null) {
            OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, charset));
            writer.write(requestBody);
            writer.flush();
            writer.close();
            outputStream.close();
        }
        urlConnection.connect();

        return urlConnection;
    }

    public static String getConsumerServiceStringByUrl(String method,
                                                String urlString, String mimeType, String charset, String requestBody)
            throws ConnectException {
        mimeType = mimeType != null && !mimeType.equals("") ? mimeType : MIME_TYPE_DEFAULT;
        charset = charset != null && !charset.equals("") ? charset : CHARSET_DEFAULT;

        URLConnection connection = null;
        try {
            urlString = getEncodeValueParamsByUrl(urlString);
            if (isAcessoSeguro(urlString)) {
                connection = (HttpsURLConnection) makeRequest(method, urlString,
                        mimeType, charset, requestBody);
                HttpsURLConnection connectionSeg = (HttpsURLConnection) connection;
                if (connectionSeg.getResponseCode() != HTTP_COD_SUCESSO) {
                    throw new WebApplicationException(connectionSeg.getResponseCode());
                }
            } else {
                connection = (HttpURLConnection) makeRequest(method, urlString,
                        mimeType, charset, requestBody);
                HttpURLConnection connectionSemSeg = (HttpURLConnection) connection;
                if (connectionSemSeg.getResponseCode() != HTTP_COD_SUCESSO) {
                    throw new WebApplicationException(connectionSemSeg.getResponseCode());
                }
            }

            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset));
            String temp, response = "";

            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            inputStream.close();
            bufferedReader.close();
            return response;
        } catch (IOException e) {
            if (e instanceof ConnectException) {
                throw new ConnectException();
            }
            e.printStackTrace();
        } finally {
            if (connection != null) {
                if (isAcessoSeguro(urlString)) {
                    ((HttpsURLConnection)connection).disconnect();
                } else {
                    ((HttpURLConnection)connection).disconnect();
                }
            }
        }
        return "";
    }

    /**
     * Realiza o encode para utf-8 dos parametros da url.
     * Obs.: Url nao aceita latin1.
     * @return
     * @throws UnsupportedEncodingException
     */
    private static String getEncodeValueParamsByUrl(String url) throws UnsupportedEncodingException {
        if (url.contains("?")) {
            String[] splitUrl = url.split("\\?");
            String paramsAll = splitUrl[1];

            String[] params = paramsAll.split("&");
            Map<String, String> mapParams = new HashMap<>();
            for (String param : params) {
                String name = param.substring(0, param.indexOf("="));
                String value = param.substring(param.indexOf("=") + 1, param.length());

                value = URLEncoder.encode(value, "utf-8");
                mapParams.put(name, value);
            }
            StringBuilder sbUrlParams = new StringBuilder(splitUrl[0]).append("?");
            int i = 0;
            for (String strHashMap : mapParams.keySet()) {
                sbUrlParams.append(strHashMap);
                sbUrlParams.append("=").append(mapParams.get(strHashMap));
                i++;

                if (mapParams.size() > i) {
                    sbUrlParams.append("&");
                }
            }
            return sbUrlParams.toString();
        } else {
            return url;
        }
    }

    private static boolean isAcessoSeguro(String url) {
        String s = url.substring(0, 5);
        if (s.contains("https")) {
            return true;
        }
        return false;
    }


}
