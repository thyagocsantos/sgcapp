package br.com.thyago.sgc.util;

import android.app.Activity;
import android.widget.Toast;

public class ToastMessagesUtil {

    public static void setToast(final Activity activity, final String msg, final boolean longDuration) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity.getBaseContext(), msg,
                        longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void setToast(final Activity activity, final int resIdMsg, final boolean longDuration) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                Toast.makeText(activity.getBaseContext(), activity.getString(resIdMsg),
                        longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
            }
        });
    }
}
