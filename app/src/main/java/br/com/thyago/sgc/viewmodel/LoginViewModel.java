package br.com.thyago.sgc.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.AsyncTask;

import com.google.gson.Gson;

import java.net.ConnectException;

import br.com.thyago.sgc.constants.StatusHttpConstants;
import br.com.thyago.sgc.exceptions.WebApplicationException;
import br.com.thyago.sgc.model.Aluno;
import br.com.thyago.sgc.util.RestUtil;

public class LoginViewModel {

    public static final String URL_LOGIN_REST = "http://18.231.168.184:8180/sgc/rest/login/logar";

    public ObservableField<String> cpf = new ObservableField<>();
    public ObservableField<String> password = new ObservableField<>();
    public ObservableBoolean loadingState = new ObservableBoolean();
    public ObservableField<StatusLogin> statusLogin = new ObservableField<>();
    public ObservableField<Aluno> alunoObservable = new ObservableField<>();

    UserLoginTask mAuthTask;

    public void onLoginClick() {
        String mCpf = cpf.get();
        String mSenha = password.get();

        if (mCpf.equals("") || mSenha.equals("")){
            loadingState.set(false);
            return;
        } else {
            loadingState.set(true);
            mAuthTask = new UserLoginTask(mCpf, mSenha);
            mAuthTask.execute();

        }
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Void> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                StringBuilder path = new StringBuilder(URL_LOGIN_REST);
                path.append("?login=").append(mEmail);
                path.append("&password=").append(mPassword);

                String jsonResponse = RestUtil.getConsumerServiceStringByUrl(RestUtil.METHOD_GET,
                        path.toString(), null, null, null);
                Aluno aluno = new Gson().fromJson(jsonResponse, Aluno.class);
                alunoObservable.set(aluno);
                statusLogin.set(StatusLogin.SUCCESSO);
            } catch (ConnectException e) {
                statusLogin.set(StatusLogin.ERRO_CONEXAO);
            } catch (WebApplicationException e) {
                if (e.getStatusCode() == StatusHttpConstants.UNAUTHORIZED) {
                    statusLogin.set(StatusLogin.NAO_AUTORIZADO);
                } else {
                    statusLogin.set(StatusLogin.ERRO_CONEXAO);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void success) {
            mAuthTask = null;
            loadingState.set(false);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

}
