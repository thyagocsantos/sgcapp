package br.com.thyago.sgc.viewmodel;

public enum StatusLogin {

    SUCCESSO,
    NAO_AUTORIZADO,
    ERRO_CONEXAO;

}
